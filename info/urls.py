# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^entrar/', 'apps.usuarios.views.entrar', name='entrar'),
	url(r'^sair/$', 'apps.usuarios.views.sair'),
    
	url(r'^$', 'apps.website.views.home_page', name='home'),
	url(r'^busca/$', 'apps.website.views.busca', name='busca'),
	url(r'^oferta_gratis/$', 'apps.website.views.oferta_gratis', name='oferta_gratis'),

	url(r'^cadastre_se/$', 'apps.usuarios.views.cadastre_se', name='cadastre_se'),
	url(r'^painel/$', 'apps.usuarios.views.painel', name='painel'),
	url(r'^estado/(?P<cidade>.*)-(?P<estado>\w+)/$', 'apps.website.views.estado', name='estado'),
	# anuncio
	url(r'^criar_anuncio_gratis/$', 'apps.anuncio.views.criar_anuncio_gratis', name='criar_anuncio_gratis'),
	url(r'^anuncio_gratis/(?P<id>\d+)/$', 'apps.anuncio.views.anuncio_gratis', name='anuncio_gratis'),
	url(r'^planos/$', 'apps.anuncio.views.planos', name='planos'),

	# habilitando a visualização dentro dos arquivos em MEDIA - não usar em produção
	url(r'^media/(.*)/$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)