# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.forms import ModelForm
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django import forms

class AnuncioGratis(models.Model):
	# ForeignKey
	usuario = models.ForeignKey(User, blank=True, null=True)
	
	# endereço
	estado = models.CharField(u'estado', max_length=2)
	cidade = models.CharField(u'cidade', max_length=100)
	endereco = models.CharField(u'endereço(logradouro)', max_length=255, help_text='Para uma melhor localização no mapa, preencha com nome da rua ou logradouro. Ex: Rua Martinho Estrela')
	numero = models.IntegerField(u'numero', help_text='numero do endereço do estabelecimento')
	bairro = models.CharField(u'bairro', max_length=50)
	cep = models.CharField(u'CEP', max_length=50)
	
	# dados do estabelecimento
	nome_empresa = models.CharField(u'nome da empresa', max_length=50)
	categoria = models.CharField(u'categoria', max_length=50, choices=(
		(u'Academias', u'Academias'), (u'Animais', u'Animais'),
		(u'assistencia Téc.', u'assistencia Téc.'), (u'Beleza/Estética', u'Beleza/Estética'),
		(u'Bares e Botecos', u'Bares e Botecos'), (u'Bebês e Crianças', u'Bebês e Crianças'),
		(u'Comunicação', u'Comunicação'), (u'Consultorias', u'Consultorias'), (u'Educação', u'Educação'),
		(u'Escritórios', u'Escritórios'), (u'Esportes', u'Esportes'), (u'Festas e Eventos', u'Festas e Eventos'),
		(u'Gráficas', u'Gráficas'), (u'Hotéis/Moteis', u'Hotéis/Moteis'), (u'Hobbies', u'Hobbies'),
		(u'Informática', u'Informática'), (u'Lazer', u'Lazer'), (u'Lanchonetes', u'Lanchonetes'),
		(u'Lojas e Varejo', u'Lojas e Varejo'), (u'Oficinas', u'Oficinas'), (u'Pizzarias', u'Pizzarias'),
		(u'Restaurantes', u'Restaurantes'), (u'Serviços', u'Serviços'), (u'Saúde/Bem Estar', u'Saúde/Bem Estar'),
		(u'Serv. Automotivos', u'Serv. Automotivos'), (u'Transportes', u'Transportes'), (u'Turismo', u'Turismo'),
		(u'Outros', u'Outros') ) )
	descricao_empresa= models.CharField(u'Breve descrição do negócio', max_length=200)
	email = models.EmailField(u'email', help_text='sera utilizado para recebimento de mensagens de usuários')
	telefone1 = models.CharField(u'telefone', max_length=50)
	telefone2 = models.CharField(u'telefone', max_length=50, blank=True, null=True)
	
	# dados do anuncio
	titulo = models.CharField(u'título do Anuncio', max_length=50)
	descricao = models.TextField(u'Descrição do Anúncio', max_length=1024)
	imagem = models.FileField(u'Imagem (150x50 pixels)', upload_to=settings.MEDIA_ROOT)
	
	# horarios
	dom = models.BooleanField(u'domingo')
	seg = models.BooleanField(u'segunda')
	ter = models.BooleanField(u'terça')
	qua = models.BooleanField(u'quarta')
	qui = models.BooleanField(u'quinta')
	sex = models.BooleanField(u'sexta')
	sab = models.BooleanField(u'sábado')
	abre = models.TimeField(u'abre às')
	fecha = models.TimeField(u'fecha às')

	# Pagamentos
	menor_preco = models.CharField(u'Menor Preço R$', max_length=6, default='1.00', help_text='Use "."(ponto) para separar as casas decimais.')
	maior_preco = models.CharField(u'Maior Preço R$', max_length=6, default='100.00', help_text='Use "."(ponto) para separar as casas decimais.')

	# redes sociais
	facebook = models.URLField(u'facebook', blank=True, null=True, default='')
	twitter = models.URLField(u'twitter', blank=True, null=True, default='')
	plus = models.URLField(u'Google+', blank=True, null=True, default='')
	insta = models.URLField(u'instagram', blank=True, null=True, default='')
	like = models.IntegerField(u'like', blank=True, null=True, default=0)
	unlike = models.IntegerField(u'unlike', blank=True, null=True, default=0)

	# Fields de auditoria
	status = models.BooleanField(u'Ativo?', default=True)
	created_on = models.DateTimeField(auto_now_add=True)
	updated_on = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.titulo

	def telefones(self):
		return "%s / %s" %(self.telefone1, self.telefone2)

	def endereco_completo(self):
		return "%s, %s - %s, %s - %s, %s" %(self.endereco, self.numero, self.bairro, self.cidade, self.estado, self.cep)

class Capa(models.Model):
    anuncio = models.OneToOneField(AnuncioGratis)
    imagem = models.FileField('imagem')

class CapaForm(ModelForm):
	class Meta:
		model = Capa
		exclude = ['anuncio']
		widgets = {'imagem' : forms.FileInput(attrs={'class':'form-control'}),}
		
class AnuncioGratisForm(ModelForm):
	class Meta:
		model = AnuncioGratis
		exclude = ['usuario', 'like', 'unlike', 'status', 'created_on', 'updated_on']
		widgets = {
			'estado' : forms.Select(attrs = {'class': 'form-control'}),
			'cidade' : forms.Select(attrs = {'class': 'form-control'}),
            'endereco' : forms.TextInput(attrs = {'class': 'form-control'}),
            'numero' : forms.NumberInput(attrs={'class':'input-group'}),
            'bairro' : forms.TextInput(attrs={'class':'form-control', 'placeholder': 'bairro'}),
            'cep' : forms.TextInput(attrs={'class':'input-group'}),
            'nome_empresa' : forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'nome da empresa/negócio'}),
            'descricao_empresa' : forms.TextInput(attrs = {'class': 'form-control'}),
            'descricao_empresa' : forms.TextInput(attrs = {'class': 'form-control'}),
            'email' : forms.TextInput(attrs = {'class': 'form-control'}),
            'telefone1' : forms.TextInput(attrs = {'class': 'input-group'}),
            'telefone2' : forms.TextInput(attrs = {'class': 'input-group'}),
            'titulo' : forms.TextInput(attrs = {'class': 'form-control'}),
            'descricao' : forms.Textarea(attrs = {'class': 'form-control'}),
            'imagem' : forms.FileInput(attrs={'class':'form-control'}),
            'dom' : forms.CheckboxInput(attrs={'class':'input-group'}),
            'seg' : forms.CheckboxInput(attrs={'class':'input-group'}),
            'ter' : forms.CheckboxInput(attrs={'class':'input-group'}),
            'qua' : forms.CheckboxInput(attrs={'class':'input-group'}),
            'qui' : forms.CheckboxInput(attrs={'class':'input-group'}),
            'sex' : forms.CheckboxInput(attrs={'class':'input-group'}),
            'sab' : forms.CheckboxInput(attrs={'class':'input-group'}),
            'menor_preco' : forms.NumberInput(attrs={'class':'input-group'}),
            'maior_preco' : forms.NumberInput(attrs={'class':'input-group'}),
            'abre' : forms.TextInput(attrs={'class':'input-group'}),
            'fecha' : forms.TextInput(attrs={'class':'input-group'}),
            'facebook' : forms.TextInput(attrs = {'class': 'form-control'}),
            'twitter' : forms.TextInput(attrs = {'class': 'form-control'}),
            'plus' : forms.TextInput(attrs = {'class': 'form-control'}),
            'insta' : forms.TextInput(attrs = {'class': 'form-control'}),
            'categoria' : forms.Select(attrs = {'class': 'input-group'}),
        }
# área para as signals
# ajustando o campo imagem no model Trabalhos para que este não capture todo o endereço da imagem
def nome_imagem(sender, instance, signal, *args, **kwargs):
	# checando se a instância possui os atributos image e id
	if instance.id and hasattr(instance, 'imagem') and not hasattr(instance, '_already_save'):
		meu_local = str(instance.imagem).split('/').pop()
		instance.imagem = meu_local
		instance._already_save = True
		instance.save()

# chamada ao signal post_save
post_save.connect(nome_imagem, sender=AnuncioGratis)
post_save.connect(nome_imagem, sender=Capa)