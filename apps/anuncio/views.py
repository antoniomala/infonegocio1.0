# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import *
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail

@login_required(login_url='/entrar/')
def criar_anuncio_gratis(request):
	if AnuncioGratis.objects.filter(status=True).filter(usuario_id=request.user).count() > 0:
		return HttpResponseRedirect(reverse('painel'))

	form = AnuncioGratisForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		form.save()
		return HttpResponseRedirect(reverse('painel'))

	return render(request, 'criar_anuncio_gratis.html', {'form': form, 'request': request})

def planos(request):
	return render(request, 'planos.html')

def anuncio_gratis(request, id):
	anuncio = AnuncioGratis.objects.get(pk=id)
	return render(request, 'anuncio_gratis.html', {'anuncio': anuncio})