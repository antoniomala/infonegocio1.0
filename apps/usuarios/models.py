# -*- coding: utf-8 -*-
from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from localflavor.br.forms import STATE_CHOICES

class Usuario(User):
	nome = models.CharField(u'nome completo', max_length=120)
	telefone = models.CharField(u'contato', max_length=50, unique=True)
	tipo_conta = models.CharField(u'tipo de conta', max_length=50, 
		choices=(('free', 'free'), ('bronze', 'bronze'), ('prata', 'prata'), ('ouro', 'ouro')), default='free')
	# Fields de auditoria
	created_on = models.DateTimeField(auto_now_add=True)
	updated_on = models.DateTimeField(auto_now=True)

class CadastrarUsuarioForm(UserCreationForm):

	class Meta:
		model = Usuario
		fields = ('username', 'email','nome', 'telefone')
		widgets = {
			'username': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Username'}),
			'email': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'E-Mail'}),
			'nome': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Nome completo'}),
			'endereco': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'endereço completo'}),
			'cep': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'CEP'}),
			'bairro': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'bairro'}),
			'telefone': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'telefone'}),

		}
	def __init__(self, *args, **kwargs):
		super(CadastrarUsuarioForm, self).__init__(*args, **kwargs)
		self.fields['password1'].widget = forms.PasswordInput(attrs={
			'class': 'form-control',
			'placeholder': '****'})
		self.fields['password2'].widget = forms.PasswordInput(attrs={
			'class': 'form-control',
			'placeholder': '****'})