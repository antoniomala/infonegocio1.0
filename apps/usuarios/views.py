# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import *
from apps.anuncio.models import AnuncioGratis
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required

def cadastre_se(request):
	form = CadastrarUsuarioForm(request.POST or None)

	if form.is_valid() and (request.POST['estado'] != '' or request.POST['cidade'] != ''):
		form = form.save(commit=False)
		form.estado = request.POST['estado']
		form.cidade = request.POST['cidade']
		form.save()
		return HttpResponseRedirect(reverse('entrar'))
	
	return render(request, 'cadastre_se.html', {'form': form})

# login no sistema
def entrar(request):
	send_mail('Teste', 'Teste de envio de email com Django', 'atrito12@gmail.com', ['atrito12@gmail.com', 'atrito12@gmail.com'])
	# checando se o usuário já realizou o login e tomanda a decisão adequada
	if request.user.is_authenticated():
		if request.user.is_superuser:
			return HttpResponseRedirect(reverse('administracao'))
		else:
			return HttpResponseRedirect(reverse('painel'))


	form = AuthenticationForm(request.POST or None)
	erro = False

	if request.method == 'POST':
		usuario = authenticate(username=request.POST['username'], password=request.POST['password'])

		if usuario is not None and usuario.is_active:
			login(request, usuario)
			
			# redirecionar para local adequado
			if usuario.is_superuser:
				return HttpResponseRedirect('/administracao/')
			else:
				# redireciona para área do cliente
				return HttpResponseRedirect('/painel/')

		else: erro = True
	
	return render(request, 'entrar.html', {'form': form, 'erro': erro})

# logout no sistema
def sair(request):
	logout(request)
	return HttpResponseRedirect(reverse('home'))

@login_required(login_url='/entrar/')
def painel(request):
	anuncio_gratis = AnuncioGratis.objects.filter(status=True).filter(usuario_id=request.user)
	return render(request, 'painel.html', {'anuncio_gratis': anuncio_gratis})

